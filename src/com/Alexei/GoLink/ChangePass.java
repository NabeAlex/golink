package com.Alexei.GoLink;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

import com.Alexei.GoLink.Adapters.Flipping;
import com.Alexei.GoLink.Connect.*;
import com.Alexei.GoLink.options.*;

import java.util.ArrayList;
import java.util.List;


public class ChangePass extends Activity implements OnClickListener {
    public ViewPager viewPager = null;

    public String user;
    private static String password;

    static SharedPreferences sPref;

    ChangePassword CH = null;

    private EditText NEW , OLD;
    private Button ACCEPT;
    private Button NEW_USER;
    public static TextView info;
    App app;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent v) {
        if(v == null) { finish(); }
        if(resultCode == RESULT_CANCELED) finish();
        SharedPreferences.Editor ed = sPref.edit();

        this.user = v.getStringExtra(Options.main_name_user);
        this.password = v.getStringExtra(Options.main_password_user);

        ed.putString(Options.main_name_user , this.user);
        ed.putString(Options.main_password_user, this.password);

        app.user = user;
        app.password = password;
        ed.commit();
        Intent in = new Intent(this , Main.class);
        in.setFlags(Intent.FILL_IN_ACTION);
        startActivity(in);
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = ((App)getApplication());
        Options.Init(this);

        LayoutInflater inflater = LayoutInflater.from(this);
        List<View> pages = new ArrayList<View>();

        View page = inflater.inflate(R.layout.menu, null);
        Controller.SET_MENU(page, this);
        pages.add(page);

        page = inflater.inflate(R.layout.change_pass, null);
        setPage2(page);
        pages.add(page);

        Flipping pagerAdapter = new Flipping(pages);
        viewPager = new ViewPager(this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(1);
        viewPager.setOnPageChangeListener(Controller.Change);

        setContentView(viewPager);
        sPref = PreferenceManager.getDefaultSharedPreferences(this);
        user = sPref.getString(Options.main_name_user, "");
        password = sPref.getString(Options.main_password_user, "");

        CH = new ChangePassword();
    }

    private void setPage2(View page) {
        Controller.menu = (ImageView)page.findViewById(R.id.menu);
        Controller.menu.setOnClickListener(this);

        NEW = (EditText)page.findViewById(R.id.new_password);
        OLD = (EditText)page.findViewById(R.id.old_password);
        NEW_USER = (Button)page.findViewById(R.id.exit);
        ACCEPT = (Button)page.findViewById(R.id.acceptChange);
        ACCEPT.setOnClickListener(this);
        NEW_USER.setOnClickListener(this);
        info = (TextView)page.findViewById(R.id.infoPASSWORD);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:
                viewPager.setCurrentItem(0);
                break;
            case R.id.back:
                viewPager.setCurrentItem(1);
                break;
            case R.id.acceptChange :
                Options.HIDE_ALL();
                info.setText("");
                String n = NEW.getText().toString();
                String o = OLD.getText().toString();
                if((n.length() < 2)||(o.length() < 2)) return;
                CH.Change(user, o, n);
                CH.start(this);
                break;
            case R.id.exit :
                Toast.makeText(this , "!" , Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, Enter.class);
                startActivityForResult(intent, 1);
                break;
            default:
                break;
        }

    }
        public static void Change(String pass) {
            SharedPreferences.Editor ed = sPref.edit();
            ChangePass.password = pass;
            ed.putString(Options.main_password_user, pass);
            ed.commit();
        }


}
