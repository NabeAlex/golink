package com.Alexei.GoLink;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;
import com.Alexei.GoLink.Connect.*;
import com.Alexei.GoLink.options.Options;


public class Add extends Activity implements View.OnClickListener {

    EditText main_link , about;
    Button gogogo;
    public TextView info = null;

    //Connect
    Insert insert = null;
    SharedPreferences sPref;
    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.add);

        sPref = PreferenceManager.getDefaultSharedPreferences(this);

        main_link = (EditText)findViewById(R.id.main_link);
        about = (EditText)findViewById(R.id.about);
        gogogo = (Button)findViewById(R.id.gogogo);
        gogogo.setOnClickListener(this);
        info = (TextView)findViewById(R.id.info_add);
    }

    @Override
    public void onClick(View v) {
        insert = new Insert();
        insert.Change(sPref.getString(Options.main_name_user, ""), sPref.getString(Options.main_password_user, ""),
                main_link.getText().toString(), about.getText().toString());
        insert.start(this);
    }

}
