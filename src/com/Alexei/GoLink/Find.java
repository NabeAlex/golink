package com.Alexei.GoLink;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.text.style.UpdateAppearance;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.Alexei.GoLink.Adapters.Flipping;
import com.Alexei.GoLink.Connect.AboutTask;
import com.Alexei.GoLink.Connect.GetRandom;
import com.Alexei.GoLink.Connect.Insert;
import com.Alexei.GoLink.Connect.ShowNear;
import com.Alexei.GoLink.options.Controller;
import com.Alexei.GoLink.options.Options;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;


public class Find extends Activity implements View.OnClickListener {
    public ViewPager viewPager = null;

    App app;
    ShowNear near;

    public TextView gps = null;
    private LocationManager Manager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = ((App) getApplicationContext());

        Options.Init(this);

        LayoutInflater inflater = LayoutInflater.from(this);
        List<View> pages = new ArrayList<View>();

        View page = inflater.inflate(R.layout.menu, null);
        Controller.SET_MENU(page, this);
        pages.add(page);

        page = inflater.inflate(R.layout.find, null);
        setPage2(page);
        pages.add(page);

        Flipping pagerAdapter = new Flipping(pages);
        viewPager = new ViewPager(this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(1);
        viewPager.setOnPageChangeListener(Controller.Change);

        //Manager = (LocationManager)getSystemService(LOCATION_SERVICE);
        setContentView(viewPager);

        near = new ShowNear();
        near.Change(app.user);
        near.start(this);

    }
    @Override
    protected void onResume() {
        super.onResume();

    }



    @Override
    protected void onPause() {
        super.onPause();

    }
    public GridView list;
    private Button on, off;
    public Button Update;
    private void setPage2(View page) {
        Controller.menu = (ImageView)page.findViewById(R.id.menu);
        Controller.menu.setOnClickListener(this);

        list = (GridView)page.findViewById(R.id.near);

        on = (Button)page.findViewById(R.id.on);
        on.setOnClickListener(this);
        off = (Button)page.findViewById(R.id.off);
        off.setOnClickListener(this);

        Update = (Button)page.findViewById(R.id.update);
        Update.setOnClickListener(this);

    }
    GetRandom gt;
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:
                viewPager.setCurrentItem(0);
                break;
            case R.id.back:
                viewPager.setCurrentItem(1);
                break;
            case R.id.update:
                near.Change(app.user);
                near.start(this);
                break;
            case R.id.on :
                startService(new Intent(this , PlaceLink.class));
                break;
            case R.id.off :
                stopService(new Intent(this, PlaceLink.class));
                break;
        }
    }

}
