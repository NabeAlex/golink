package com.Alexei.GoLink;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

import com.Alexei.GoLink.Adapters.Flipping;
import com.Alexei.GoLink.Connect.*;
import com.Alexei.GoLink.options.*;

import java.util.ArrayList;
import java.util.List;

public class Main extends Activity implements OnClickListener {
    public ViewPager viewPager = null;

    EditText linker;
    Button go, btnLoad;
    public GridView links;
    LinearLayout home;


    public static String user = null;
    String password = null;
    public static String cash = ""; public static boolean LOAD_fromCash = false;
    //Sql
    public SharedPreferences sPref;

    //Connect
    static Show show = null;
    Delete del = null;
    final String SAVED_TEXT = "saved_text";


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = ((App)getApplication());
        show = new Show("");

        Options.Init(this);

        LayoutInflater inflater = LayoutInflater.from(this);
        List<View> pages = new ArrayList<View>();

        View page = inflater.inflate(R.layout.menu, null);
        Controller.SET_MENU(page, this);
        pages.add(page);

        page = inflater.inflate(R.layout.main, null);
        setPage2(page);
        pages.add(page);

        Flipping pagerAdapter = new Flipping(pages);

        viewPager = new ViewPager(this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(1);

        setContentView(viewPager);
        linker.clearFocus();
        viewPager.setOnPageChangeListener(Controller.Change);

        sPref = PreferenceManager.getDefaultSharedPreferences(this);


        if(app.user.equals("")) {
            this.user = sPref.getString(Options.main_name_user, "");
            this.password = sPref.getString(Options.main_password_user, "");
            this.cash = sPref.getString(Options.default_user, "");
            if (!this.cash.equals("")) {
                this.LOAD_fromCash = true;
            }
            if ((this.user == "") || (this.password == "")) {
                this.intent_to_reg();
            }
        }else{
            this.user = app.user;
            this.password = app.password;

            linker.setText(app.sweet);
            linker.setSelection(linker.getText().length());
            show.Change(app.sweet);
            show.start(this);
            app.sweet = app.user;
        }
    }
    protected void Text(String st) {
        Toast.makeText(this, st , Toast.LENGTH_SHORT).show();
    }
    protected void setPage2(View page) {
        go = (Button)page.findViewById(R.id.search);
        btnLoad = (Button)page.findViewById(R.id.add_btn);

        Controller.menu = (ImageView)page.findViewById(R.id.menu);
        Controller.menu.setOnClickListener(this);

        go.setOnClickListener(this);
        btnLoad.setOnClickListener(this);
        linker = (EditText)page.findViewById(R.id.Linker);

        linker.setText(this.user);

        links = (GridView)page.findViewById(R.id.link1);
        registerForContextMenu(links);

        home = (LinearLayout)page.findViewById(R.id.home);
        home.setOnClickListener(this);

        del = new Delete();

        linker.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_DOWN)
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        Options.HIDE_ALL();
                        String text = linker.getText().toString();
                        text = text.trim();
                        if((text == null)||(text.length() == 0)) { linker.setText(user); }
                        String s = ((text == null) || (text.length() == 0)) ? user : text;
                        linker.setSelection(linker.getText().length());
                        show.Change(s);
                        start_show();
                        return true;
                    }
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search :
                Options.HIDE_ALL();
                String text = linker.getText().toString();
                text = text.trim();
                if((text == null)||(text.length() == 0)) { linker.setText(this.user); }
                String s = ((text == null)||(text.length() == 0)) ? this.user :  text;
                linker.setSelection(linker.getText().length());
                show.Change(s);
                show.start(this);
                break;
            case R.id.add_btn :
                Intent intent = new Intent(this , Add.class);
                startActivity(intent);
                break;
            case R.id.home :
                linker.setText(app.user);
                linker.setSelection(linker.getText().length());
                show.Change(app.user);
                show.start(this);
                break;
            case R.id.menu :
                viewPager.setCurrentItem(0);
                break;
            case R.id.back:
                viewPager.setCurrentItem(1);
                break;
        }
    }
    App app;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null) { finish(); }
        if(resultCode == RESULT_CANCELED) finish();
        Editor ed = sPref.edit();

        this.user = data.getStringExtra(Options.main_name_user);
        this.password = data.getStringExtra(Options.main_password_user);

        app.user = user;
        app.password = password;

        ed.putString(Options.main_name_user , this.user);
        ed.putString(Options.main_password_user, this.password);
        show.Change(this.user);
        show.start(this);

        ed.commit();
    }

    public void intent_to_reg() {
        Intent intent = new Intent(this, Enter.class);
        startActivityForResult(intent, 1);
    }

    public void start_show() {
        show.start(this);
    }

    private static final int CM_DELETE_ID = 1;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, CM_DELETE_ID, 0, "Удалить запись");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            Show.list.remove(acmi.position);
            del.Change(app.user , app.password , Show.ad.getCount() - acmi.position);
            del.start("");
            return true;
        }
        return super.onContextItemSelected(item);
    }

}
