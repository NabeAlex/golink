package com.Alexei.GoLink;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.Alexei.GoLink.Singleton.*;
import com.Alexei.GoLink.options.Options;

public class App extends Application {
    public String user = "";
    public String password = "";
    public String sweet = "";
    @Override
    public void onCreate() {

        super.onCreate();

        sPref = PreferenceManager.getDefaultSharedPreferences(this);
        this.user = sPref.getString(Options.main_name_user, "");
        this.sweet = this.user;
        this.password = sPref.getString(Options.main_password_user, "");

        AppStorage.initInstance();

    }
    public void setUser(String s) {
        this.sweet = s;
    }

    public SharedPreferences sPref;

}
