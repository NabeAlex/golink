package com.Alexei.GoLink.options;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.*;
import android.widget.Button;
import android.widget.ImageView;
import com.Alexei.GoLink.*;
import com.Alexei.GoLink.Random;

import java.util.*;

public class Controller {
    static private final int MAX = 10;
    static public ImageView back = null , menu = null;
    static public List<View> MENU_PLACES = new ArrayList<View>();

    private static View view = null;
    public static void SET_MENU(View page, final Activity act) {
        Controller.back = (ImageView)page.findViewById(R.id.back);
        Controller.back.setOnClickListener((View.OnClickListener) act);

        view = page.findViewById(R.id.Write_PAGE);
        if(act.getClass().equals(About.class)) view.setBackgroundColor(R.drawable.design_menu);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(act.getClass().equals(About.class)) return;
                Intent in = new Intent(act , About.class);
                in.setFlags(Intent.FILL_IN_ACTION);
                act.startActivity(in);
                act.finish();
            }
        });
        MENU_PLACES.add(view);

        view = page.findViewById(R.id.Main_PAGE);
        if(act.getClass().equals(Main.class)) view.setBackgroundColor(R.drawable.design_menu);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(act.getClass().equals(Main.class)) return;
                Intent in = new Intent(act , Main.class);
                in.setFlags(Intent.FILL_IN_ACTION);
                act.startActivity(in);
                act.finish();
            }
        });
        MENU_PLACES.add(view);

        view = page.findViewById(R.id.Show_PAGE);
        if(act.getClass().equals(ShowFriends.class)) view.setBackgroundColor(R.drawable.design_menu);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(act.getClass().equals(ShowFriends.class)) return;
                Intent in = new Intent(act , ShowFriends.class);
                in.setFlags(Intent.FILL_IN_ACTION);
                act.startActivity(in);
                act.finish();
            }
        });
        MENU_PLACES.add(view);

        view = page.findViewById(R.id.Change_PAGE);
        if(act.getClass().equals(ChangePass.class)) view.setBackgroundColor(R.drawable.design_menu);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(act.getClass().equals(ChangePass.class)) return;
                Intent in = new Intent(act , ChangePass.class);
                in.setFlags(Intent.FILL_IN_ACTION);
                act.startActivity(in);
                act.finish();
            }
        });
        MENU_PLACES.add(view);

        view = page.findViewById(R.id.Find_PAGE);
        if(act.getClass().equals(Find.class)) view.setBackgroundColor(R.drawable.design_menu);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(act.getClass().equals(Find.class)) return;
                Intent in = new Intent(act , Find.class);
                in.setFlags(Intent.FILL_IN_ACTION);
                act.startActivity(in);
                act.finish();
            }
        });
        MENU_PLACES.add(view);

        view = page.findViewById(R.id.Random_PAGE);
        if(act.getClass().equals(Random.class)) view.setBackgroundColor(R.drawable.design_menu);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(act.getClass().equals(Random.class)) return;
                Intent in = new Intent(act , Random.class);
                in.setFlags(Intent.FILL_IN_ACTION);
                act.startActivity(in);
                act.finish();
            }
        });
        MENU_PLACES.add(view);

    }

    static public ViewPager.OnPageChangeListener Change = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i2) {
            return;
        }
        @Override
        public void onPageSelected(int i) {
            if(i == 0) {
                Options.HIDE_ALL();
                Controller.back.setBackgroundResource(R.drawable.ic_hardware_keyboard_arrow_right);
                Controller.menu.setBackgroundColor(0x0106000d);
            }else {
                Controller.back.setBackgroundColor(0x0106000d);
                Controller.menu.setBackgroundResource(R.drawable.menu);
            }
            return;
        }

        @Override
        public void onPageScrollStateChanged(int i) {
            return;
        }
    };

}
