package com.Alexei.GoLink.options;

import android.app.Activity;
import android.content.Context;
import android.support.v4.hardware.display.DisplayManagerCompat;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.Alexei.GoLink.Items.Item;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class Options {
    static public Activity act;

    static public final String site = "http://www.golink.besaba.com/";
    static public final String accept = "Succesfully!";

    static public final String main_name_user = "User";
    static public final String main_password_user = "Password";
    static public final String default_user = "Default";

    static public float scale;

    static public void Init(Activity a) {
        Options.act = (Activity)a;
    }

    static public String censored(String str) {
        str = str.replace("_" , " ");
        str = str.replace("(" , "");
        str = str.replace(")" , "");
        str = str.replace(" " , "%20");
        return str;
    }
    static public String censored_user(String str) {
        str = str.replace("_" , "");
        str = str.replace("(" , "");
        str = str.replace(")" , "");
        str = str.replace(" " , "");
        str = str.replace("&" , "");
        str = str.replace("=" , "");
        str = str.replace("?" , "");
        return str;
    }
    static public ArrayList getJSON(String s , int CHANGE) {
        ArrayList<Item> list = new ArrayList<Item>();
        byte[] link = null; byte[] about = null;
        try {

            JSONArray ar = new JSONArray(s); JSONArray test;
            if(CHANGE == 1) {
                for (int i = ar.length() - 1; i >= 0; i--) {
                    link = ar.getString(i).getBytes();
                    about = "".getBytes();

                    list.add(new Item(new String(link), new String(about)));
                }
            }
            else if(CHANGE == 2) {
                for (int i = ar.length() - 1; i >= 0; i--) {
                    JSONArray child = ar.getJSONArray(i);
                    link = child.getString(0).getBytes();
                    about = child.getString(1).getBytes();

                    list.add(new Item(new String(link), new String(about)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    static public boolean CHECK_response(String result) {
        result = result.trim();
        if(!((result == null)||(result.length() == 0))) return true;

        return false;
    }
    static public void HIDE_ALL() {
        Options.K();
    }
    public static void K() {
        Log.i("!" , "!");
        try {
            InputMethodManager imm = (InputMethodManager) Options.act.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(Options.act.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }catch(Exception e) {

        }
    }
}
