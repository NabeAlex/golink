package com.Alexei.GoLink;

import android.content.Intent;
import android.text.Editable;
import android.view.Window;
import  com.Alexei.GoLink.options.Options;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.methods.HttpGet;


public class Enter extends Activity implements OnClickListener {

    private EditText login;
    private EditText password;
    private TextView info;

    private Button button;

    private MyAsyncTask mytask = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter);

        this.button = (Button)findViewById(R.id.enter);
        button.setOnClickListener(this);
        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);
        info = (TextView)findViewById(R.id.info);
    }

    @Override
    public void onClick(View v) {
        if(this.mytask == null) {
            info.setText("Connecting");
            this.mytask = new MyAsyncTask();
            String login_ = Options.censored_user(login.getText().toString());
            String password_ = Options.censored_user(password.getText().toString());
            mytask.execute(Options.site + "reg?", login_, password_);
        }
    }

    public class MyAsyncTask extends AsyncTask<String, String, String > {
        private String userTask = null;
        private String passwordTask = null;
        @Override
        protected String doInBackground(String... uri) {
            this.userTask = uri[1];
            this.passwordTask = uri[2];
            String response;

                try {
                    DefaultHttpClient hc = new DefaultHttpClient();
                    ResponseHandler<String> res = new BasicResponseHandler();
                    HttpGet getMethod = new HttpGet(uri[0] + "user=" + uri[1] + "&" + "password=" + uri[2]);
                    response = hc.execute(getMethod, res);
                } catch ( Exception e) {
                    response = "";
                }
            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            result = result.trim();
            if(!((result == null)||(result.length() == 0))) {
                Intent intent = new Intent();
                intent.putExtra(Options.main_name_user , this.userTask);
                intent.putExtra(Options.main_password_user , this.passwordTask);
                setResult(RESULT_OK, intent);
                finish();
                info.setText("Okey ^_^");
            }else {
                info.setText("Oh, sorry, fatal error :(");
            }
            mytask = null;
        }

    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

}
