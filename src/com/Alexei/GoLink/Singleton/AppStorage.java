package com.Alexei.GoLink.Singleton;

public class AppStorage {
    private static AppStorage mInstance;
    private String MyVariable;


    public static void initInstance() {
        if (mInstance == null) {
            mInstance = new AppStorage();
        }
    }

    public static AppStorage getInstance() {
        return mInstance;
    }

    private AppStorage() {
        MyVariable = "This is my Variable";
    }

    public String getMyVariable() {
        return MyVariable;
    }

    public void setMyVariable(String var) {
        MyVariable = var;
    }
}

