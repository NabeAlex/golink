package com.Alexei.GoLink;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.*;
import android.view.animation.LinearInterpolator;
import android.widget.*;
import com.Alexei.GoLink.Adapters.Flipping;
import com.Alexei.GoLink.Connect.Delete;
import com.Alexei.GoLink.Connect.GetRandom;
import com.Alexei.GoLink.Connect.Show;
import com.Alexei.GoLink.Speed.*;

import android.os.Handler;
import com.Alexei.GoLink.options.Controller;
import com.Alexei.GoLink.options.Options;

import java.util.ArrayList;
import java.util.List;
import android.view.View.OnClickListener;

public class Random extends Activity implements SensorEventListener, OnClickListener {
    GetRandom j;

    Handler h;

    private SensorManager sm;

    private static final long UPDATE_INTERVAL = 500;
    private static final long MEASURE_TIMES = 20;
    public TextView tv , i;
    int counter;
    private Calc mdXYZ;

    /*public Random() {
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }*/
    public ViewPager viewPager = null;

    public String cache = "";
    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);

        Options.Init(this);

        LayoutInflater inflater = LayoutInflater.from(this);
        List<View> pages = new ArrayList<View>();

        View page = inflater.inflate(R.layout.menu, null);
        Controller.SET_MENU(page, this);
        pages.add(page);

        page = inflater.inflate(R.layout.random, null);
        setPage2(page);
        pages.add(page);

        Flipping pagerAdapter = new Flipping(pages);
        viewPager = new ViewPager(this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(1);
        viewPager.setOnPageChangeListener(Controller.Change);

        setContentView(viewPager);


        viewPager.setOnPageChangeListener(Controller.Change);
        j = new GetRandom();
        j.start(this);

        sm = (SensorManager) getSystemService(SENSOR_SERVICE);

        if(sm.getSensorList(Sensor.TYPE_ACCELEROMETER).size()!=0){
            Sensor s = sm.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
            sm.registerListener(this,s, SensorManager.SENSOR_DELAY_NORMAL);
        }else{
            l.setOnClickListener(this);
        }
        Ini();
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mDisplay = mWindowManager.getDefaultDisplay();
    }

    Button l = null;
    protected void setPage2(View page) {
        tv = (TextView)page.findViewById(R.id.txt);
        i = (TextView)page.findViewById(R.id.i);
        l = (Button)page.findViewById(R.id.up);
        l.setOnClickListener(this);
        Controller.menu = (ImageView)page.findViewById(R.id.menu);
        Controller.menu.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sm.getSensorList(Sensor.TYPE_ACCELEROMETER).size() != 0){
            Sensor s = sm.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
            sm.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    private void Ini() {
        mSensorX = 0;
        mSensorY = 0;
        max = 0;
        float default_ = 17.f;
    }
    private float mSensorX = 0;
    private float mSensorY = 0;
    private float max = 0;
    private float default_ = 17.f;

    private Display mDisplay;
    private WindowManager mWindowManager;

    private boolean flag = true;
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER)
            return;

        switch (mDisplay.getRotation()) {
            case Surface.ROTATION_0:
                mSensorX = event.values[0];
                mSensorY = event.values[1];
                if(mSensorX > max) max = mSensorX;
                if((flag) && (default_ < Math.abs(mSensorX))) {
                    New();
                }
                break;
            case Surface.ROTATION_90:

                break;
            case Surface.ROTATION_180:

                break;
            case Surface.ROTATION_270:

                break;

        }
    }
    Activity me = this;
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.up :
                if(flag)
                    New();
                break;
            case R.id.menu :
                viewPager.setCurrentItem(0);
                break;
            case R.id.back:
                viewPager.setCurrentItem(1);
                break;
        }
    }
    int count = 0;
    void New() {
        if((!cache.equals("")&&(!cache.equals(tv.getText())))) {
            tv.setText(cache);
            cache = "";
            count = 0;
        }else if(cache.equals(tv.getText())) {
            count++;
            tv.setText(cache + "(" + count + " раз)");
            cache = "";
        }else{
            tv.setText("Error :(");
            count = 0;
        }
        flag = false;
        j.start(this);
        i.setText("2.0");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                i.setText("1.5");
            }
        }, 500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                i.setText("1.0");
            }
        }, 1000);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                i.setText("0.5");
            }
        }, 1500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                i.setText(""); flag = true;
            }
        }, 2000);
    }
}
