package com.Alexei.GoLink.Connect;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;
import com.Alexei.GoLink.Add;
import com.Alexei.GoLink.ChangePass;
import com.Alexei.GoLink.Main;
import com.Alexei.GoLink.options.Options;
import org.apache.http.HttpConnection;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ChangePassword {
    protected String user = null;
    protected String password = null;
    protected static String link = null;

    InsertAsyncTask task = null;


    public ChangePassword() {

    }

    public void Change(String user , String password , String new_pass) {
        this.user = user;
        this.password = password;
        this.link = new_pass;
        this.link = Options.censored(this.link);
    }


    public void start(ChangePass activity) {
        this.task = new InsertAsyncTask();
        this.task.link(activity);
        this.task.execute(Options.site + "change?", this.user, this.password, this.link);
        return;
    }


    static class InsertAsyncTask extends AsyncTask<String , String , String> {

        ChangePass activity;

        void link(ChangePass act) {
            activity = act;
        }

        void unLink() {
            activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... uri) {
            String response = null;
            try {
                DefaultHttpClient hc = new DefaultHttpClient();
                final HttpParams httpParameters = hc.getParams();
                HttpConnectionParams.setSoTimeout(httpParameters , 3000);
                ResponseHandler<String> res = new BasicResponseHandler();
                HttpGet getMethod = new HttpGet(uri[0] +
                        "user=" + uri[1] + "&" +
                        "password=" + uri[2] + "&" +
                        "newpass=" + uri[3]);
                response = hc.execute(getMethod, res);
            }catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(Options.CHECK_response(result)) {
                activity.Change(ChangePassword.link);
                activity.info.setText("Удачно!");
            }else{
                activity.info.setText("Ошибка");
            }
        }

    }
}
