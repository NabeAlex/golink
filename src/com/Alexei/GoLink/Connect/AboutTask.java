package com.Alexei.GoLink.Connect;

import android.app.Activity;
import android.widget.Toast;
import com.Alexei.GoLink.About;
import com.Alexei.GoLink.Items.*;
import com.Alexei.GoLink.Adapters.*;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.Alexei.GoLink.Main;
import com.Alexei.GoLink.options.Options;
import android.os.AsyncTask;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Arrays;

public class AboutTask {
    static boolean CAN = true;

    static public String user = null;
    static public String password = null;
    static public String about = null;

    static public String function = null;
    public ShowAsyncTask task;

    public AboutTask() {

    }
    public void Change(String user, String password, String about) { this.user = user;this.about = about;
        this.password = password;
    }
    public void start(About activity) {
        if(!CAN) return; else CAN = false;
        task = new ShowAsyncTask();
        task.link(activity);
        task.execute(Options.site + "about?" , this.user , this.password , this.about);
        return;
    }



    public static class ShowAsyncTask extends AsyncTask<String , String , String> {
        MyAdapter ad;

        About activity;

        public void link(About act) {
            activity = act;
        }


        void unLink() {
            activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... uri) {
            String response;
            try {
                DefaultHttpClient hc = new DefaultHttpClient();
                ResponseHandler<String> res = new BasicResponseHandler();
                HttpGet getMethod = new HttpGet(uri[0] + "user=" + uri[1] + "&password="
                                                +uri[2] + "&about=" + uri[3] );
                response = hc.execute(getMethod, res);
            }catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            CAN = true;
            if(Options.CHECK_response(result)) {
                //Toast.makeText(activity.getApplicationContext() , result , Toast.LENGTH_LONG).show();
                activity.info.setText("Удачно!");
            }else{
                activity.info.setText("Не получается!");
            }

        }

    }
}
