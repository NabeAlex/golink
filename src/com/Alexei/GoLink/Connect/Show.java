package com.Alexei.GoLink.Connect;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.Alexei.GoLink.Items.*;
import com.Alexei.GoLink.Adapters.*;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.Alexei.GoLink.Main;
import com.Alexei.GoLink.options.Options;
import android.os.AsyncTask;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;

import java.util.ArrayList;
import java.util.Arrays;

public class Show {

    static boolean CAN = true;

    static public String user = null;
    static public String function = null;
    public ShowAsyncTask task;

    public Show(String user) {
        this.user = user;
    }
    public void Change(String user) { this.user = user; }
    public void start(Main activity) {
        if(!CAN) return; else CAN = false;
        task = new ShowAsyncTask();
        task.link(activity);
        task.execute(Options.site + "show?" , this.user);
        act = activity;
        return;
    }
    public Main act = null;

    public void start() {
        task = new ShowAsyncTask();
        task.link(act);
        String func = (function == null) ? "show?" : function;
        task.execute(Options.site + func , this.user);
        return;
    }

    static public MyAdapter ad;
    static public ArrayList list;

  public static class ShowAsyncTask extends AsyncTask<String , String , String> {


      private OnItemClickListener itemListener = new OnItemClickListener() {

          @Override
          public void onItemClick(AdapterView<?> parent, View v, int pos,
                                  long id) {
              Item text = (Item)parent.getItemAtPosition(pos);

              String get = text.getHeader();
              Uri uri;
              if((get.length() > 6)&&(get.substring(0 , 6) == "http://")) {
                  uri = Uri.parse(text.getHeader());
              }else{
                  Toast.makeText(activity.getApplicationContext() , "https://www.m.google.ru/?text="+ get + "#newwindow=1&q=" + get , Toast.LENGTH_LONG ).show();
                  uri = Uri.parse("https://www.google.ru/?text="+ get + "#newwindow=1&q=" + get);
              }
              try {
                  activity.startActivity(new Intent(Intent.ACTION_VIEW, uri));
              }catch (Exception e){

              }finally {

              }
          }
      };

      Main activity;

      public void link(Main act) {
          activity = act;
      }


      void unLink() {
          activity = null;
      }
      public SharedPreferences sPrefsPref;
      boolean cash = false;
      String cash_STRING = "";
        @Override
        protected void onPreExecute() {
            sPrefsPref = PreferenceManager.getDefaultSharedPreferences(activity);
            super.onPreExecute();
            cash_STRING = (activity.LOAD_fromCash) ? activity.cash : ""; cash = activity.LOAD_fromCash;
            list = new ArrayList<Item>();
            list.add(new Item("Loading..." , ""));
            activity.links.setAdapter(new MyAdapter(activity, list));
        }

        @Override
        protected String doInBackground(String... uri) {
            if(cash) return cash_STRING;
            String response;
            try {

                HttpClient hc = new DefaultHttpClient();
                hc.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT , 5000);
                ResponseHandler<String> res = new BasicResponseHandler();
                HttpPost post = new HttpPost();
                //post.addHeader();
                HttpGet getMethod = new HttpGet(uri[0] + "user=" + uri[1]);
                response = hc.execute(getMethod, res);
            }catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            CAN = true;
            if(Options.CHECK_response(result)) {
              list = Options.getJSON(result , 2);
              ad = new MyAdapter(activity , list);
              activity.links.setAdapter(ad);
              if(activity.user == user) {
                  SharedPreferences.Editor ed = sPrefsPref.edit();
                  ed.putString(Options.default_user , result);
              }
                activity.links.setOnItemClickListener(this.itemListener);
          }else {
                list = new ArrayList<Item>();
                list.add(new Item("Nothing", ""));
                ad = new MyAdapter(activity, list);
                activity.links.setAdapter(ad);
            }

        }

    }
}
