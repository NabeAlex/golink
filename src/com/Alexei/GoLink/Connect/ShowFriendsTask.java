package com.Alexei.GoLink.Connect;

import android.content.Intent;
import android.widget.Toast;
import com.Alexei.GoLink.App;
import com.Alexei.GoLink.Items.*;
import com.Alexei.GoLink.Adapters.*;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.Alexei.GoLink.Main;
import com.Alexei.GoLink.ShowFriends;
import com.Alexei.GoLink.options.Options;
import android.os.AsyncTask;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

import java.util.ArrayList;
import java.util.Arrays;

public class ShowFriendsTask {
    static boolean CAN = true;

    static public String user = null;
    static public String password = null;

    public ShowAsyncTask task;

    public ShowFriendsTask() {}

    public void Change(String user , String pass) { this.user = user; this.password = pass; }
    public void start(ShowFriends activity) {
        if(!CAN) return; else CAN = false;
        task = new ShowAsyncTask();
        task.link(activity);
        task.execute(Options.site + "friend?" , this.user , this.password);
        return;
    }
    public static App APPLICATION;
    public static ArrayList<Item> list;
    public static MyAdapter ad;
    public static class ShowAsyncTask extends AsyncTask<String , String , String> {


        OnItemLongClickListener itemLongListener = new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v,
                                           int position, long id) {

                return true;
            }
        };

        private OnItemClickListener itemListener = new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int pos,
                                    long id) {

                Item text = (Item)parent.getItemAtPosition(pos);
                String get = text.getHeader();
                APPLICATION = ((App) activity.getApplicationContext());
                APPLICATION.setUser(get);
                try {
                    Intent in = new Intent(activity , Main.class);
                    in.setFlags(Intent.FILL_IN_ACTION);
                    activity.startActivity(in);
                    activity.finish();
                }catch (Exception e){

                }finally {

                }
            }
        };

        ShowFriends activity;

        public void link(ShowFriends act) {
            activity = act;
        }
        void unLink() {
            activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            list = new ArrayList<Item>();
            list.add(new Item("Loading..." , ""));

            activity.friends.setAdapter(new MyAdapter(activity, list));

        }

        @Override
        protected String doInBackground(String... uri) {
            String response;
            try {
                DefaultHttpClient hc = new DefaultHttpClient();
                ResponseHandler<String> res = new BasicResponseHandler();
                HttpGet getMethod = new HttpGet(uri[0] + "user=" + uri[1] + "&password=" + uri[2]);
                response = hc.execute(getMethod, res);
            }catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            CAN = true;
            if(Options.CHECK_response(result)) {
                list = Options.getJSON(result , 1);
                ad = new MyAdapter(activity , list);
                activity.friends.setAdapter(ad);
                activity.friends.setOnItemClickListener(this.itemListener);
            }else{
                list = new ArrayList<Item>();
                list.add(new Item("Nothing" , ""));
                ad = new MyAdapter(activity , list);

                activity.friends.setAdapter(ad);
            }


        }

    }
}
