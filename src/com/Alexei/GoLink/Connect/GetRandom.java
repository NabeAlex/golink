package com.Alexei.GoLink.Connect;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.Alexei.GoLink.Items.*;
import com.Alexei.GoLink.Adapters.*;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.Alexei.GoLink.Main;
import com.Alexei.GoLink.Random;
import com.Alexei.GoLink.options.Options;
import android.os.AsyncTask;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;

import java.util.ArrayList;
import java.util.Arrays;

public class GetRandom {

    static boolean CAN = true;

    static public String function = null;
    public ShowAsyncTask task;

    public GetRandom() {

    }
    public void start(Random activity) {
        if(!CAN) return; else CAN = false;
        task = new ShowAsyncTask();
        task.link(activity);
        task.execute(Options.site + "random");
        return;
    }


    public static class ShowAsyncTask extends AsyncTask<String , String , String> {
        MyAdapter ad;

        Random activity;

        public void link(Random act) {
            activity = act;
        }

        void unLink() {
            activity = null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... uri) {
            String response;
            try {

                HttpClient hc = new DefaultHttpClient();
                hc.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT , 5000);
                ResponseHandler<String> res = new BasicResponseHandler();
                HttpGet getMethod = new HttpGet(uri[0]);
                response = hc.execute(getMethod, res);
            }catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            CAN = true;
            if(Options.CHECK_response(result)) {
                activity.cache = result;
            }else {
                activity.cache = "OOPS!";
            }

        }

    }
}

