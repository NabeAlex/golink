package com.Alexei.GoLink.Connect;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;
import com.Alexei.GoLink.Add;
import com.Alexei.GoLink.Main;
import com.Alexei.GoLink.options.Options;
import org.apache.http.HttpConnection;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Insert {
    protected String user = null;
    protected String password = null;
    protected String link = null;
    protected String about = null;

    InsertAsyncTask task = null;


    public Insert() {

    }

    public void Change(String user , String password , String link , String about) {
        this.user = user;
        this.password = password;
        link = Options.censored(link);
        about = Options.censored(about);
        this.link = link;
        this.about = about;
    }


    public void start(Add activity) {
        activity.info.setText("Идет запрос...");
        this.task = new InsertAsyncTask();
        this.task.link(activity);

        this.task.execute(Options.site + "insert?", this.user, this.password, this.link, this.about);
        Toast.makeText(activity , this.user , Toast.LENGTH_LONG).show();


        return;
    }


    static class InsertAsyncTask extends AsyncTask<String , String , String> {

        Add activity;

        void link(Add act) {
            activity = act;
        }

        void unLink() {
            activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... uri) {
            String response = null;
            try {
                DefaultHttpClient hc = new DefaultHttpClient();
                final HttpParams httpParameters = hc.getParams();
                HttpConnectionParams.setSoTimeout(httpParameters , 3000);
                ResponseHandler<String> res = new BasicResponseHandler();
                HttpGet getMethod = new HttpGet(uri[0] +
                                                "user=" + uri[1] + "&" +
                                                "password=" + uri[2] + "&" +
                                                "link=" + uri[3] + "&" +
                                                "about=" + uri[4]);
                response = hc.execute(getMethod, res);
            }catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(Options.CHECK_response(result)) {
                activity.info.setText("Okey ^_^");
                activity.finish();
            }else{
                activity.info.setText("Oh, sorry, fatal error :(");
                Toast.makeText(activity.getApplicationContext() , "ERROR" , Toast.LENGTH_SHORT).show();

            }
            activity.finish();
        }

    }
}
