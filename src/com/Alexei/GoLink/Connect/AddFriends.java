package com.Alexei.GoLink.Connect;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;
import com.Alexei.GoLink.Add;
import com.Alexei.GoLink.Main;
import com.Alexei.GoLink.ShowFriends;
import com.Alexei.GoLink.options.Options;
import org.apache.http.HttpConnection;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class AddFriends {
    protected String user = null;
    protected String password = null;
    protected String link = null;

    InsertAsyncTask task = null;


    public AddFriends() {

    }

    public void Change(String user , String password , String link) {
        this.user = user;
        this.password = password;
        link = Options.censored(link);
        this.link = link;
    }


    public void start(ShowFriends activity) {
        this.task = new InsertAsyncTask();
        this.task.link(activity);

        this.task.execute(Options.site + "addfriend?", this.user, this.password, this.link);
        return;
    }


    static class InsertAsyncTask extends AsyncTask<String , String , String> {

        ShowFriends activity;

        void link(ShowFriends act) {
            activity = act;
        }

        void unLink() {
            activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... uri) {
            String response = null;
            try {
                DefaultHttpClient hc = new DefaultHttpClient();
                final HttpParams httpParameters = hc.getParams();
                HttpConnectionParams.setSoTimeout(httpParameters , 3000);
                ResponseHandler<String> res = new BasicResponseHandler();
                HttpGet getMethod = new HttpGet(uri[0] +
                        "user=" + uri[1] + "&" +
                        "password=" + uri[2] + "&" +
                        "friend=" + uri[3]);
                response = hc.execute(getMethod, res);
            }catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(Options.CHECK_response(result)) {
                activity.Update();
            }else{
                Toast.makeText(activity.getApplicationContext() , "Не получилось" , Toast.LENGTH_SHORT).show();
            }
        }

    }
}
