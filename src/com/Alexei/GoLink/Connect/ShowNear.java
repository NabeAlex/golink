package com.Alexei.GoLink.Connect;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.Alexei.GoLink.App;
import com.Alexei.GoLink.Find;
import com.Alexei.GoLink.Items.*;
import com.Alexei.GoLink.Adapters.*;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.Alexei.GoLink.Main;
import com.Alexei.GoLink.options.Options;
import android.os.AsyncTask;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;

import java.util.ArrayList;
import java.util.Arrays;

public class ShowNear {

    static boolean CAN = true;

    static public String user = null;
    public ShowAsyncTask task;


    public void Change(String user) { this.user = user; }
    public void start(Find activity) {
        if(!CAN) return; else CAN = false;
        task = new ShowAsyncTask();
        task.link(activity);
        task.execute(Options.site + "near?" , this.user);
        return;
    }
    public Find act = null;

    public void start() {
        if(!CAN)
        task = new ShowAsyncTask();
        task.link(act);
        task.execute(Options.site + "near?" , this.user);
        return;
    }

    public static App APPLICATION;
    public static class ShowAsyncTask extends AsyncTask<String , String , String> {
        MyAdapter ad;

        private OnItemClickListener itemListener = new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int pos,
                                    long id) {
                Item text = (Item)parent.getItemAtPosition(pos);
                String get = text.getHeader();
                APPLICATION = ((App) activity.getApplicationContext());
                APPLICATION.setUser(get);
                try {
                    Intent in = new Intent(activity , Main.class);
                    in.setFlags(Intent.FILL_IN_ACTION);
                    activity.startActivity(in);
                    activity.finish();
                }catch (Exception e){

                }finally {

                }
            }
        };

        Find activity;

        public void link(Find act) {
            activity = act;
        }


        void unLink() {
            activity = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ArrayList<Item> list = new ArrayList<Item>();
            list.add(new Item("Loading..." , ""));

            activity.list.setAdapter(new MyAdapter(activity, list));

        }

        @Override
        protected String doInBackground(String... uri) {
            String response;
            try {

                HttpClient hc = new DefaultHttpClient();
                hc.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT , 5000);
                ResponseHandler<String> res = new BasicResponseHandler();
                HttpPost post = new HttpPost();
                HttpGet getMethod = new HttpGet(uri[0] + "user=" + uri[1]);
                response = hc.execute(getMethod, res);
            }catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result); // [["tttt",""]]
            CAN = true;
            if(Options.CHECK_response(result)) {
                ArrayList<Item> list = Options.getJSON(result , 2);
                activity.list.setAdapter(new MyAdapter(activity , list));

                activity.list.setOnItemClickListener(this.itemListener);
            }else {
                ArrayList<Item> list = new ArrayList<Item>();
                list.add(new Item("No people!", ""));
                ad = new MyAdapter(activity, list);
                activity.list.setAdapter(ad);
            }

        }

    }
}