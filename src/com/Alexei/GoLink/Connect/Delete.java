package com.Alexei.GoLink.Connect;

import android.os.AsyncTask;
import android.widget.Toast;
import com.Alexei.GoLink.Add;
import com.Alexei.GoLink.Main;
import com.Alexei.GoLink.options.Options;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Delete {
    protected String user = null;
    protected String password = null;
    protected int num = -1;

    DeleteAsyncTask task = null;
    public static boolean dowork = false;

    public Delete() {

    }

    public void Change(String user , String password , int pos) {
        this.user = user;
        this.password = password;
        this.num = pos;
    }

    private static String under = null;

    public void start(String under) {
        this.task = new DeleteAsyncTask();
        this.under = under;
        this.task.execute(Options.site + "delete" + under + "?", this.user, this.password, Integer.toString(this.num));

        return;
    }


    static class DeleteAsyncTask extends AsyncTask<String , String , String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dowork = true;
        }

        @Override
        protected String doInBackground(String... uri) {
            String response = null;
            try {
                DefaultHttpClient hc = new DefaultHttpClient();
                ResponseHandler<String> res = new BasicResponseHandler();
                HttpGet getMethod = new HttpGet(uri[0] +
                        "user=" + uri[1] + "&" +
                        "password=" + uri[2] + "&" +
                        "num=" + uri[3]);
                response = hc.execute(getMethod, res);
            }catch (Exception e) {
                response = "";
            }
            return uri[0] +
                    "user=" + uri[1] + "&" +
                    "password=" + uri[2] + "&" +
                    "num=" + uri[3];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dowork = false;
            if(under.equals("")) {
                Show.ad.notifyDataSetChanged();
            }else{
                ShowFriendsTask.ad.notifyDataSetChanged();
            }

        }

    }
}
