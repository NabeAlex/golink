package com.Alexei.GoLink.Connect;

import android.os.AsyncTask;
import com.Alexei.GoLink.options.Options;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;


public class SendGPS {
    protected String user = null;
    protected String lat;
    protected String lon;

    InsertAsyncTask task = null;


    public SendGPS() {

    }

    public void Change(String user ,String lat , String lon) {
        this.user = user;
        this.lat = lat;
        this.lon = lon;
    }


    public void start() {
        this.task = new InsertAsyncTask();
        this.task.execute(Options.site + "gps?", this.user, this.lat, this.lon);
        return;
    }


    static class InsertAsyncTask extends AsyncTask<String , String , String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... uri) {
            String response = null;
            try {
                DefaultHttpClient hc = new DefaultHttpClient();
                final HttpParams httpParameters = hc.getParams();
                HttpConnectionParams.setSoTimeout(httpParameters, 3000);
                ResponseHandler<String> res = new BasicResponseHandler();
                HttpGet getMethod = new HttpGet(uri[0] +
                        "user=" + uri[1] + "&" +
                        "lat=" + uri[2] + "&" +
                        "lon=" + uri[3]);
                response = hc.execute(getMethod, res);
            }catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }

    }
}

