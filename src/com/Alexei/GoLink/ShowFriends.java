package com.Alexei.GoLink;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.Alexei.GoLink.Adapters.Flipping;
import com.Alexei.GoLink.Connect.*;
import com.Alexei.GoLink.options.Controller;
import com.Alexei.GoLink.options.Options;

import java.util.ArrayList;
import java.util.List;


public class ShowFriends extends Activity implements View.OnClickListener {

    public ViewPager viewPager = null;

    public GridView friends;
    SharedPreferences sPref;

    private String user = null, password = null;

    public static ShowFriendsTask show = null;
    public static AddFriends addF = null;

    public EditText ed = null;
    Button go = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Options.Init(this);
        app = ((App)getApplication());
        del = new Delete();
        LayoutInflater inflater = LayoutInflater.from(this);
        List<View> pages = new ArrayList<View>();

        View page = inflater.inflate(R.layout.menu, null);
        Controller.SET_MENU(page, this);
        pages.add(page);

        page = inflater.inflate(R.layout.showfriends, null);
        setPage2(page);
        pages.add(page);

        Flipping pagerAdapter = new Flipping(pages);
        viewPager = new ViewPager(this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(1);
        viewPager.setOnPageChangeListener(Controller.Change);

        setContentView(viewPager);

        sPref = PreferenceManager.getDefaultSharedPreferences(this);
        user = sPref.getString(Options.main_name_user, "");
        password = sPref.getString(Options.main_password_user, "");

        //****DEFAULT****//
        addF = new AddFriends();

        show = new ShowFriendsTask();
        show.Change(user , password);
        show.start(this);
    }

    private void setPage2(View page) {
        Controller.menu = (ImageView)page.findViewById(R.id.menu);
        Controller.menu.setOnClickListener(this);
        friends = (GridView)page.findViewById(R.id.friends);
        registerForContextMenu(friends);

        go = (Button)page.findViewById(R.id.ADD);
        go.setOnClickListener(this);
        ed = (EditText)page.findViewById(R.id.EditAdd);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:
                viewPager.setCurrentItem(0);
                break;
            case R.id.back:
                viewPager.setCurrentItem(1);
                break;
            case R.id.ADD :
                Options.HIDE_ALL();
                if(ed.getText().toString().length() == 0) return;
                String s = ed.getText().toString();
                ed.setText("");
                s = Options.censored_user(s);
                addF.Change(user , password , s);
                addF.start(this);
                break;
        }
    }
    public void Update() {
        show.start(this);
    }


    private static final int CM_DELETE_ID = 1;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, CM_DELETE_ID, 0, "Удалить запись");
    }
    Delete del;
    App app;
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            ShowFriendsTask.list.remove(acmi.position);
            del.Change(app.user , app.password , ShowFriendsTask.ad.getCount() - acmi.position);
            del.start("f");
            return true;
        }
        return super.onContextItemSelected(item);
    }
}
