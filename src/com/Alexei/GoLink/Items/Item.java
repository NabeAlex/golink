package com.Alexei.GoLink.Items;

/**
 * Created by user on 14.08.2014.
 */
public class Item {
    public String header;

    public String subHeader;

    public Item(String h, String s){
        this.header=h;
        this.subHeader=s;
    }

    public String getHeader() {
        return header;
    }
    public void setHeader(String header) {
        this.header = header;
    }
    public String getSubHeader() {
        return subHeader;
    }
    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

}
