package com.Alexei.GoLink;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.Alexei.GoLink.Adapters.Flipping;
import com.Alexei.GoLink.Connect.AboutTask;
import com.Alexei.GoLink.Connect.Insert;
import com.Alexei.GoLink.options.Controller;
import com.Alexei.GoLink.options.Options;

import java.util.ArrayList;
import java.util.List;


public class About extends Activity implements View.OnClickListener {
    public ViewPager viewPager = null;

    public EditText Eabout;
    public Button Babout;

    public static AboutTask AT = null;
    private String user;
    private String password;

    SharedPreferences sPref;

    public TextView info = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Options.Init(this);

        LayoutInflater inflater = LayoutInflater.from(this);
        List<View> pages = new ArrayList<View>();

        View page = inflater.inflate(R.layout.menu, null);
        Controller.SET_MENU(page, this);
        pages.add(page);

        page = inflater.inflate(R.layout.about, null);
        setPage2(page);
        pages.add(page);

        Flipping pagerAdapter = new Flipping(pages);
        viewPager = new ViewPager(this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(1);
        viewPager.setOnPageChangeListener(Controller.Change);

        setContentView(viewPager);

        sPref = PreferenceManager.getDefaultSharedPreferences(this);
        user = sPref.getString(Options.main_name_user, "");
        password = sPref.getString(Options.main_password_user, "");

        AT = new AboutTask();
    }

    private void setPage2(View page) {
        Controller.menu = (ImageView)page.findViewById(R.id.menu);
        Controller.menu.setOnClickListener(this);
        Babout = (Button)page.findViewById(R.id.accept);
        Eabout= (EditText)page.findViewById(R.id.aboutEdit);
        Babout.setOnClickListener(this);

        info = (TextView)page.findViewById(R.id.infoABOUT);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:
                viewPager.setCurrentItem(0);
                break;
            case R.id.back:
                viewPager.setCurrentItem(1);
                break;
            case R.id.accept :
                Options.HIDE_ALL();
                String s = Eabout.getText().toString();
                s = s.replace("flagnew" , "");
                s = s.replace("\n" , "flagnew");
                if(s.length() < 2) return;
                s = Options.censored(s);
                AT.Change(user , password , s);
                AT.start(this);
                break;
        }
    }

}
