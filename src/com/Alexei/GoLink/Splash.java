package com.Alexei.GoLink;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.Alexei.GoLink.Main;
import com.Alexei.GoLink.R;

public class Splash extends Activity {

    private static final int SPLASH_SHOW_TIME = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splah);

        //new BackgroundSplashTask().execute();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Splash.this,
                        Main.class);
                startActivity(i);
                finish();
            }
        }, 2000);


    }

    private class BackgroundSplashTask extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Intent i = new Intent(Splash.this,
                    Main.class);
            startActivity(i);
            finish();
        }

    }
}