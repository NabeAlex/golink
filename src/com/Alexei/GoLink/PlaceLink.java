package com.Alexei.GoLink;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import com.Alexei.GoLink.Connect.SendGPS;

public class PlaceLink extends Service
{
    private float lat , lon;
    private int count;
    LocationManager Manager = null;
    private PlaceLink me;

    private static final String TAG = "BOOMBOOMTESTGPS";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    private static SendGPS send;
    private class LocationListener implements android.location.LocationListener{
        Location mLastLocation;
        public LocationListener(String provider)
        {
            mLastLocation = new Location(provider);
        }
        @Override
        public void onLocationChanged(Location location)
        {
            showLocation(location);
            mLastLocation.set(location);
        }
        @Override
        public void onProviderDisabled(String provider)
        {

        }
        @Override
        public void onProviderEnabled(String provider)
        {

        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {

        }
    }
    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    private void showLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(
                LocationManager.NETWORK_PROVIDER)) {
                count++;
                lat = (float) location.getLatitude();
                lon = (float) location.getLongitude();
                Toast.makeText(me , lat + " " + lon , Toast.LENGTH_SHORT).show();
                send.Change(user , lat + "" , lon + "");
                send.start();
                count = 0;

        }
    }

    @Override
    public IBinder onBind(Intent arg0)
    {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }
    private static String user;
    @Override
    public void onCreate() {
        user = ((App)this.getApplication()).user;
        send = new SendGPS();
        count = 0;
        lon = 0.0f;
        lat = 0.0f;
        Manager = (LocationManager) getSystemService(LOCATION_SERVICE);
        me = this;

        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {

        } catch (IllegalArgumentException ex) {

        }
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.e(TAG, "onStartCommand!");
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {

                }
            }
        }
    }
    private void initializeLocationManager() {
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }
}
